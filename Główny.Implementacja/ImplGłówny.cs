﻿using Główny.Kontrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyświetlacz.Kontrakt;

namespace Główny.Implementacja
{
    public class ImplGłówny : IGłówny
    {
        IWyświetlacz wys;
        int liczbam = 0;
        int liczbak = 0;
        public ImplGłówny(IWyświetlacz wys)
        {
            this.wys = wys;
        }


        public void DodajM()
        {
            liczbam++;
            Odśwież();
        }

        public void DodajK()
        {
            liczbak++;
            Odśwież();
        }

        public void Odśwież()
        {
            this.wys.Tekst("M="+liczbam.ToString()+" K="+liczbak.ToString());
        }
        public void Run()
        {
            this.Odśwież();
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    this.DodajK();
                }
                else if (key.Key == ConsoleKey.Decimal)
                {
                    this.DodajM();
                }

            } while (key.Key != ConsoleKey.Escape);
        }
    }
}
