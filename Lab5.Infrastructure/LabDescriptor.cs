﻿using KontenerTW;
using System;
using System.Reflection;
using Wyświetlacz.Kontrakt;
using Wyświetlacz.Implementacja;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Główny.Kontrakt.IGłówny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Główny.Implementacja.ImplGłówny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Wyświetlacz.Kontrakt.IWyświetlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyświetlacz.Implementacja.Wyświetlacz));

        #endregion
    }
}
