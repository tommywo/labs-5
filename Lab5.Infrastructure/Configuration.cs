﻿using System;
using PK.Container;
using KontenerTW;
using Wyświetlacz.Kontrakt;
using Wyświetlacz.Implementacja;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container kontener = new Container();
            kontener.Register(typeof(Wyświetlacz.Implementacja.Wyświetlacz));
            kontener.Register(typeof(Główny.Implementacja.ImplGłówny));
            return kontener;


        }
    }
}
