﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyświetlacz.Kontrakt;
using Lab5.DisplayForm;


namespace Wyświetlacz.Implementacja
{
    public class Wyświetlacz : IWyświetlacz
    {
        DisplayViewModel model;

        public Wyświetlacz()
        {
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);
        }
        
        // wyświetlenie widoku

        public void Tekst(string tekst)
        {
            this.model.Text = tekst;
        }
    }
}
