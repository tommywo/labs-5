﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyświetlacz.Kontrakt
{
    public interface IWyświetlacz
    {
        void Tekst(string tekst);
    }
}
