﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Główny.Kontrakt
{
    public interface IGłówny
    {
        void DodajM();
        void DodajK();
        void Odśwież();
        void Run();
    }
}
